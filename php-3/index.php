<?php
require('animal.php');
require('ape.php');
require('frog.php');

// RELEASE 0 
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded? : " . $sheep->cold_blooded . "<br>"; // "no"

echo "<br>";
// RELEASE 1
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // "shaun"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded? : " . $kodok->cold_blooded . "<br> Jump: "; // "no
$kodok->jump(); // "hop hop

echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // "shaun"
echo "legs : " . $sungokong->legs . "<br>"; // 4
echo "cold blooded? : " . $sungokong->cold_blooded . "<br> Yell: "; // "no
$sungokong->yell(); // "Auooo"
