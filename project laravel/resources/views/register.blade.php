<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="welcome">
        <div>
            <h1>Buat Akun Baru!</h1>
            <h3>Sign Up Form</h3>
            <div>
                <div>
                    <label for="nama">First name:</label>
                    <br>
                    <input type="text" name="nama" id="">
                </div>
                <br>
                <div>
                    <label for="nama1">Last name:</label>
                    <br>
                    <input type="text" name="nama1" id="">
                </div>
            </div>
            <div>
                <br>
                <label for="gender">Gender:</label><br>
                <input type="radio" id="male" name="gender" value="male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label>
            </div>
            <br>
            <div>
                <label for="national">Nationality:</label>
                <br>
                <select name="nationality" id="">
                    <option value="Indonesian">Indonesian</option>
                    <option value="American">American</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <br>
            <div>
                <label for="gender">Gender:</label><br>
                <input type="checkbox" id="male" name="gender" value="male">
                <label for="male">Bahasa Indonesia</label><br>
                <input type="checkbox" id="female" name="gender" value="female">
                <label for="female">Nigeria</label><br>
                <input type="checkbox" id="other" name="gender" value="other">
                <label for="other">Other</label>
            </div>
            <br>
            <div>
                <label for="textarea">Bio:</label><br>
                <textarea name="textarea" id="" cols="30" rows="10"></textarea>
            </div>
            <button type="submit">Sign Up</button>
    </form>
    </div>
</body>

</html>