<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Routing\Matcher\RedirectableUrlMatcher;

class CastController extends Controller
{
    //
    public function create()
    {
        # code...
        return view('cast.cast');
    }
    public function store(Request $request)
    {
        # code...
        // dd($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'umur' => 'required',
        //     'bio' => 'required'
        // ]);
        $query = DB::table('cast')->insert([
            'nama' => $request['name'],
            'umur' => $request['age'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Posted Successfully');
    }
    public function index()
    {
        # code...
        $post = DB::table('cast')->get();
        return view('cast.index', compact('post'));
    }
    public function show($id)
    {
        # code...
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('post'));
    }
    public function edit($id)
    {
        # code...
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('post'));
    }
    public function update($id, Request $request)
    {
        # code...
        $query = DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['name'],
                    'umur' => $request['age'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('/cast')->with('success', 'Pembaharuan tersimpan');
    }
    public function destroy($id)
    {
        # code...
        $query = DB::table('cast')
            ->where('id', $id)
            ->delete();
        return redirect('/cast')->with('success', 'Deleted Successfully');
    }
}
