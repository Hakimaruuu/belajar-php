@extends('adminlte.master')
@section('content')
    <div class="mr-4 ml-4">
        <div class="card">
            <h4>Nama: {{$post->nama}} </h4>
            <p>Umur: {{$post->umur}} </p>
            <p>Bio: {{$post->bio}} </p>
        </div>
        <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
    </div>
@endsection