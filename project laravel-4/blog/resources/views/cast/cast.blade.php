@extends('adminlte.master')
@section('content')
<div class="ml-4 mr-4">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Masukkan Data Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Cast Name</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', '') }}" id="1" placeholder="Enter name">
              @error('name')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputUmur1">Cast Age</label>
                <input type="text" name="age" class="form-control" value="" id="2" placeholder="Enter age">
                @error('age')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputBio1">Biography Cast</label>
                <input type="text" name="bio" class="form-control" id="3" placeholder="Enter bio">
                @error('bio')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
</div>
@endsection