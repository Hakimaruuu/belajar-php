@extends('adminlte.master')
@section('content')
    <div class="mt-4 mr-4">
        @if (session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
        @endif
        <div class="card-header">
            <h3 class="card-title">Cast Table</h3>
        </div>
        <div class="card-body">
            <a href="/cast/create" class="btn mb-2 btn-primary">Create New Cast</a>
            <table class="table table-bordered">
              <thead>                  
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cast</th>
                  <th>Age</th>
                  <th style="width: 40px">Label</th>
                </tr>
              </thead>
              <tbody>
                  @forelse ($post as $item => $p)
                  <tr>
                    <td>{{ $item + 1}}</td>
                    <td>{{$p->nama}} </td>
                    <td>{{$p->umur}}</td>
                    <td>
                        <a href="/cast/{{$p->id}}" class="btn btn-default btn-sm">More</a>
                        <a href="/cast/{{$p->id}}/edit" class="btn mt-1 btn-info btn-sm">Edit</a>
                        <form action="/cast/{{$p->id}}" method='post'>
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                  </tr>
                      
                  @empty
                      <tr>
                          <td colspan="4" align="center">Empty posts</td>
                      </tr>
                  @endforelse
                {{-- <tr>
                  <td>2.</td>
                  <td>Clean database</td>
                  <td>
                    <div class="progress progress-xs">
                      <div class="progress-bar bg-warning" style="width: 70%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-warning">70%</span></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Cron job running</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar bg-primary" style="width: 30%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-primary">30%</span></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Fix and squish bugs</td>
                  <td>
                    <div class="progress progress-xs progress-striped active">
                      <div class="progress-bar bg-success" style="width: 90%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-success">90%</span></td>
                </tr> --}}
              </tbody>
            </table>
          </div>
    </div>
@endsection